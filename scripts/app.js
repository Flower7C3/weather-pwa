// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


(function() {
  'use strict';

  var app = {
    isLoading: true,
    visibleCards: {},
    selectedCities: [],
    spinner: document.querySelector('.loader'),
    cardTemplate: document.querySelector('.cardTemplate'),
    cityTemplate: document.querySelector('.cityTemplate'),
    container: document.querySelector('.main'),
    addDialog: document.querySelector('.dialog-container-add'),
    cityDialog: document.querySelector('.dialog-container-select'),
    errorDialog: document.querySelector('.dialog-container-error'),
    errorDialogBody: document.querySelector('.dialog-container-error .dialog-body'),
    daysOfWeek: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  };


  /*****************************************************************************
   *
   * Event listeners for UI elements
   *
   ****************************************************************************/

  document.getElementById('butRefresh').addEventListener('click', function() {
    // Refresh all of the forecasts
    app.updateForecasts();
  });

  document.getElementById('butAdd').addEventListener('click', function() {
    // Open/show the add new city dialog
    app.toggleAddDialog(true);
  });

  document.getElementById('butCityFind').addEventListener('click', function() {
    // Add the newly selected city
    var input = document.getElementById('cityName');
    var label = input.value;
    app.getCities(label);
    app.toggleAddDialog(false);
    app.toggleCityDialog(true);
  });

  document.getElementById('butCityFindCancel').addEventListener('click', function() {
    // Close the add new city dialog
    app.toggleAddDialog(false);
  });

  app.butCityAddEvent = function() {
    // Add the newly selected city
    var key = this.querySelector('.city-key').textContent;
    var label = this.querySelector('.location').textContent;
    app.getForecast(key, label);
    app.addSelectedCity(key, label);
    app.toggleCityDialog(false);
  };

  app.butCityRemoveEvent = function() {
    // Add the newly selected city
    var key = this.parentNode.querySelector('.city-key').textContent;
    var label = this.parentNode.querySelector('.location').textContent;
    var card = app.visibleCards[key];
    if (card) {
      app.container.removeChild(card);
      delete app.visibleCards[card];
      var selectedCities = app.removeSelectedCity(key, label);
    }
  };

  document.getElementById('butCityAddCancel').addEventListener('click', function() {
    app.toggleCityDialog(false);
  });

  document.getElementById('butErrorOk').addEventListener('click', function() {
    app.toggleErrorDialog(false);
  });



  /*****************************************************************************
   *
   * Methods to update/refresh the UI
   *
   ****************************************************************************/

  // Toggles the visibility of the add new city dialog.
  app.toggleAddDialog = function(visible) {
    console.log('app.toggleAddDialog($visible='+visible+')')
    if (visible) {
      app.addDialog.classList.add('dialog-container--visible');
    } else {
      app.addDialog.classList.remove('dialog-container--visible');
    }
  };

  app.toggleCityDialog = function(visible) {
    console.log('app.toggleCityDialog($visible='+visible+')')
    if (visible) {
      app.cityDialog.classList.add('dialog-container--visible');
    } else {
      app.cityDialog.classList.remove('dialog-container--visible');
    }
  };

  app.toggleErrorDialog = function(visible, message) {
    console.log('app.toggleErrorDialog($visible='+visible+',$message='+message+')')
    if (visible) {
      app.errorDialog.classList.add('dialog-container--visible');
      app.errorDialogBody.innerHTML = message;
    } else {
      app.errorDialog.classList.remove('dialog-container--visible');
    }
  };

  app.toggleSpinner = function(visible){
    console.log('app.toggleSpinner($visible='+visible+')')
    if (visible) {
      if (!app.isLoading) {
        app.spinner.removeAttribute('hidden');
        app.container.setAttribute('hidden', true);
        app.isLoading = true;
      }
    }else{
      if (app.isLoading) {
        app.spinner.setAttribute('hidden', true);
        app.container.removeAttribute('hidden');
        app.isLoading = false;
      }
    }
  }


  app.updateCitiesSelect = function(places) {
    console.log('app.updateCitiesSelect($places)')
    app.toggleSpinner(true);
    app.cityDialog.querySelector('.dialog-body').textContent = '';
    places.forEach(function(data) {
      var cityOption = ""
      var btn = app.cityTemplate.cloneNode(true);
      btn.classList.remove('cityTemplate');
      btn.querySelector('.city-key').textContent = data.woeid;
      btn.querySelector('.location').textContent = data.name + ', ' + data.country.content;
      btn.querySelector('.type').textContent = data.placeTypeName.content;
      btn.removeAttribute('hidden');
      btn.addEventListener('click', app.butCityAddEvent, false);
      app.cityDialog.querySelector('.dialog-body').appendChild(btn);
    });
    app.toggleSpinner(false);
  };

  // Updates a weather card with the latest weather forecast. If the card
  // doesn't already exist, it's cloned from the template.
  app.updateForecastCard = function(data) {
    console.log('app.updateForecastCard($data)')
    app.toggleSpinner(true);
    var dataLastUpdated = new Date(data.created);
    var sunrise = data.channel.astronomy ? data.channel.astronomy.sunrise : '';
    var sunset = data.channel.astronomy ? data.channel.astronomy.sunset : '';
    var current = data.channel.item.condition;
    var humidity = data.channel.atmosphere.humidity;
    var wind = data.channel.wind;

    var card = app.visibleCards[data.key];
    if (!card) {
      card = app.cardTemplate.cloneNode(true);
      card.classList.remove('cardTemplate');
      card.querySelector('.location').textContent = data.label;
      card.querySelector('.city-key').textContent = data.key;
      card.removeAttribute('hidden');
      app.container.appendChild(card);
      app.visibleCards[data.key] = card;
    }

    // Verifies the data provide is newer than what's already visible
    // on the card, if it's not bail, if it is, continue and update the
    // time saved in the card
    var cardLastUpdatedElem = card.querySelector('.card-last-updated');
    var cardLastUpdated = cardLastUpdatedElem.textContent;
    if (cardLastUpdated) {
      cardLastUpdated = new Date(cardLastUpdated);
      // Bail if the card has more recent data then the data
      if (dataLastUpdated.getTime() < cardLastUpdated.getTime()) {
        return;
      }
    }
    cardLastUpdatedElem.textContent = data.created;

    card.querySelector('.description').textContent = current.text;
    card.querySelector('.date').textContent = current.date;
    card.querySelector('.current .icon').classList.add(app.getIconClass(current.code));
    card.querySelector('.current .temperature .value').textContent =
      Math.round(current.temp);
    card.querySelector('.current .sunrise').textContent = sunrise;
    card.querySelector('.current .sunset').textContent = sunset;
    card.querySelector('.current .humidity').textContent =
      Math.round(humidity) + '%';
    card.querySelector('.current .wind .value').textContent =
      Math.round(wind.speed);
    card.querySelector('.current .wind .direction').textContent = wind.direction;
    var nextDays = card.querySelectorAll('.future .oneday');
    var today = new Date();
    today = today.getDay();
    for (var i = 0; i < 7; i++) {
      var nextDay = nextDays[i];
      var daily = data.channel.item.forecast[i];
      if (daily && nextDay) {
        nextDay.querySelector('.date').textContent =
          app.daysOfWeek[(i + today) % 7];
        nextDay.querySelector('.icon').classList.add(app.getIconClass(daily.code));
        nextDay.querySelector('.temp-high .value').textContent =
          Math.round(daily.high);
        nextDay.querySelector('.temp-low .value').textContent =
          Math.round(daily.low);
      }
    }
    card.querySelector('.close').addEventListener('click', app.butCityRemoveEvent, false)
    app.toggleSpinner(false);
  };


  /*****************************************************************************
   *
   * Methods for dealing with the model
   *
   ****************************************************************************/

  /*
   * Gets a forecast for a specific city and updates the card with the data.
   * getForecast() first checks if the weather data is in the cache. If so,
   * then it gets that data and populates the card with the cached data.
   * Then, getForecast() goes to the network for fresh data. If the network
   * request goes through, then the card gets updated a second time with the
   * freshest data.
   */
  app.getCities = function(label) {
    console.log('app.getCities($label='+label+')')
    var statement = 'select * from geo.places where text="'+label+'"'
    var url = 'https://query.yahooapis.com/v1/public/yql?format=json&q=' + statement;
    // Fetch the latest data.
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState === XMLHttpRequest.DONE) {
        if (request.status === 200) {
          var response = JSON.parse(request.response);
          if(response.query.count > 0){
            var places = [response.query.results.place]
            if(response.query.count > 1){
              places = response.query.results.place;
            }
            app.updateCitiesSelect(places);
          } else {
            app.toggleCityDialog(false);
            app.toggleErrorDialog(true, 'No cities found.')
          }
        }
      } 
    };
    request.open('GET', url);
    request.send();
  };
  app.getForecast = function(key, label) {
    console.log('app.getForecast($key='+key+',$label='+label+')')
    var statement = 'select * from weather.forecast where woeid = ' + key + ' and u="c"';
    var url = 'https://query.yahooapis.com/v1/public/yql?format=json&q=' + statement;
    // TODO add cache logic here
    if ('caches' in window) {
      /*
       * Check if the service worker has already cached this city's weather
       * data. If the service worker has the data, then display the cached
       * data while the app fetches the latest data.
       */
      caches.match(url).then(function(response) {
        if (response) {
          response.json().then(function updateFromCache(json) {
            var results = json.query.results;
            results.label = label;
            results.key = key;
            results.created = json.query.created;
            app.updateForecastCard(results);
          });
        }
      });
    }
    // Fetch the latest data.
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState === XMLHttpRequest.DONE) {
        if (request.status === 200) {
          var response = JSON.parse(request.response);
          var results = response.query.results;
          results.label = label;
          results.key = key;
          results.created = response.query.created;
          app.updateForecastCard(results);
        }
      } else {
        // Return the initial weather forecast since no data is available.
        // app.updateForecastCard(initialWeatherForecast);
      }
    };
    request.open('GET', url);
    request.send();
  };

  // Iterate all of the cards and attempt to get the latest forecast data
  app.updateForecasts = function() {
    console.log('app.updateForecasts()')
    var keys = Object.keys(app.visibleCards);
    keys.forEach(function(key) {
      app.getForecast(key);
    });
  };

  // TODO add saveSelectedCities function here
  // Save list of cities to localStorage.
  app.addSelectedCity = function(key, label) {
    console.log('app.addSelectedCity($key='+key+', $label='+label+')')
    var selectedCities = app.getSelectedCities();
    selectedCities.push({key: key, label: label})
    localStorage.selectedCities = JSON.stringify(selectedCities);
  };

  app.removeSelectedCity = function(key, label){
    console.log('app.removeSelectedCity($key='+key+', $label='+label+')')
    var selectedCities = app.getSelectedCities()
    Object.values(selectedCities).forEach(function(city,id){
      if(city.key == key){
        selectedCities.splice(id, 1)
      }
    });
    localStorage.selectedCities = JSON.stringify(selectedCities);
  };

  app.getSelectedCities = function(){
    console.log('app.getSelectedCities()')
    var selectedCities = localStorage.selectedCities;
    if (selectedCities) {
      selectedCities = JSON.parse(selectedCities);
    }else{
      selectedCities = [];
    }
    return selectedCities;
  }

  app.getIconClass = function(weatherCode) {
    console.log('app.getIconClass($weatherCode='+weatherCode+')')
    // Weather codes: https://developer.yahoo.com/weather/documentation.html#codes
    weatherCode = parseInt(weatherCode);
    switch (weatherCode) {
      case 25: // cold
      case 32: // sunny
      case 33: // fair (night)
      case 34: // fair (day)
      case 36: // hot
      case 3200: // not available
        return 'clear-day';
      case 0: // tornado
      case 1: // tropical storm
      case 2: // hurricane
      case 6: // mixed rain and sleet
      case 8: // freezing drizzle
      case 9: // drizzle
      case 10: // freezing rain
      case 11: // showers
      case 12: // showers
      case 17: // hail
      case 35: // mixed rain and hail
      case 40: // scattered showers
        return 'rain';
      case 3: // severe thunderstorms
      case 4: // thunderstorms
      case 37: // isolated thunderstorms
      case 38: // scattered thunderstorms
      case 39: // scattered thunderstorms (not a typo)
      case 45: // thundershowers
      case 47: // isolated thundershowers
        return 'thunderstorms';
      case 5: // mixed rain and snow
      case 7: // mixed snow and sleet
      case 13: // snow flurries
      case 14: // light snow showers
      case 16: // snow
      case 18: // sleet
      case 41: // heavy snow
      case 42: // scattered snow showers
      case 43: // heavy snow
      case 46: // snow showers
        return 'snow';
      case 15: // blowing snow
      case 19: // dust
      case 20: // foggy
      case 21: // haze
      case 22: // smoky
        return 'fog';
      case 24: // windy
      case 23: // blustery
        return 'windy';
      case 26: // cloudy
      case 27: // mostly cloudy (night)
      case 28: // mostly cloudy (day)
      case 31: // clear (night)
        return 'cloudy';
      case 29: // partly cloudy (night)
      case 30: // partly cloudy (day)
      case 44: // partly cloudy
        return 'partly-cloudy-day';
    }
  };

  /*
   * Fake weather data that is presented when the user first uses the app,
   * or when the user has not saved any cities. See startup code for more
   * discussion.
   */
  var initialWeatherForecast = {
    // key: '2459115',
    // label: 'New York, NY',
    // created: '2016-07-22T01:00:00Z',
    // channel: {
    //   astronomy: {
    //     sunrise: "5:43 am",
    //     sunset: "8:21 pm"
    //   },
    //   item: {
    //     condition: {
    //       text: "Windy",
    //       date: "Thu, 21 Jul 2016 09:00 PM EDT",
    //       temp: 56,
    //       code: 24
    //     },
    //     forecast: [
    //       {code: 44, high: 86, low: 70},
    //       {code: 44, high: 94, low: 73},
    //       {code: 4, high: 95, low: 78},
    //       {code: 24, high: 75, low: 89},
    //       {code: 24, high: 89, low: 77},
    //       {code: 44, high: 92, low: 79},
    //       {code: 44, high: 89, low: 77}
    //     ]
    //   },
    //   atmosphere: {
    //     humidity: 56
    //   },
    //   wind: {
    //     speed: 25,
    //     direction: 195
    //   }
    // }
  };
  // TODO uncomment line below to test app with fake data
  // app.updateForecastCard(initialWeatherForecast);

  /************************************************************************
   *
   * Code required to start the app
   *
   * NOTE: To simplify this codelab, we've used localStorage.
   *   localStorage is a synchronous API and has serious performance
   *   implications. It should not be used in production applications!
   *   Instead, check out IDB (https://www.npmjs.com/package/idb) or
   *   SimpleDB (https://gist.github.com/inexorabletash/c8069c042b734519680c)
   ************************************************************************/

  // TODO add startup code here
  var selectedCities= app.getSelectedCities();
  if (Object.keys(selectedCities).length > 0) {
    selectedCities.forEach(function(city) {
      app.getForecast(city.key, city.label);
    });
  } else {
    /* The user is using the app for the first time, or the user has not
     * saved any cities, so show the user some fake data. A real app in this
     * scenario could guess the user's location via IP lookup and then inject
     * that data into the page.
     */
    // app.updateForecastCard(initialWeatherForecast);
    // selectedCities = [
      // {key: initialWeatherForecast.key, label: initialWeatherForecast.label}
    // ];
    // app.saveSelectedCities();
  }

  app.toggleSpinner(false);

  // TODO add service worker code here
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register('./service-worker.js')
             .then(function() { console.log('Service Worker Registered'); });
  }
})();
